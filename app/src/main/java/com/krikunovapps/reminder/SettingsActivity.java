package com.krikunovapps.reminder;

import android.content.Context;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.RingtonePreference;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

public class SettingsActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_top);
        setSupportActionBar(toolbar);

        getFragmentManager().beginTransaction()
                .replace(R.id.content, new SettingsFragment()).commit();
    }

    public static class SettingsFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            final Context context = getActivity().getApplicationContext();
            addPreferencesFromResource(R.xml.preferences);

            // ringtone
            final RingtonePreference ringtonePreference = (RingtonePreference) getPreferenceScreen().getPreference(0);
            Ringtone ringtone = RingtoneManager.getRingtone(context, Prefs.getRingtone(context));
            ringtonePreference.setSummary(ringtone.getTitle(context));
            ringtonePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    Ringtone ringtone = RingtoneManager.getRingtone(context, Uri.parse((String) newValue));
                    ringtonePreference.setSummary(ringtone.getTitle(context));
                    return true;
                }
            });

            // snooze time
            final ListPreference snoozeTimePreference = (ListPreference) getPreferenceScreen().getPreference(1);
            String snoozeTime = snoozeTimePreference.getEntry().toString();
            snoozeTimePreference.setSummary(snoozeTime);
            snoozeTimePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String newSnoozeTime = (String) newValue;
                    int index = snoozeTimePreference.findIndexOfValue(newSnoozeTime);
                    snoozeTimePreference.setSummary(snoozeTimePreference.getEntries()[index]);
                    return true;
                }
            });

            // default day
            final ListPreference defaultDayPreference = (ListPreference) getPreferenceScreen().getPreference(2);
            String defaultDay = defaultDayPreference.getEntry().toString();
            defaultDayPreference.setSummary(defaultDay);
            defaultDayPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String newDefaultDay = (String) newValue;
                    int index = defaultDayPreference.findIndexOfValue(newDefaultDay);
                    defaultDayPreference.setSummary(defaultDayPreference.getEntries()[index]);
                    return true;
                }
            });

            // default time
            final ListPreference defaultTimePreference = (ListPreference) getPreferenceScreen().getPreference(3);
            String defaultTime = defaultTimePreference.getEntry().toString();
            defaultTimePreference.setSummary(defaultTime);
            defaultTimePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String newDefaultTime = (String) newValue;
                    int index = defaultTimePreference.findIndexOfValue(newDefaultTime);
                    defaultTimePreference.setSummary(defaultTimePreference.getEntries()[index]);
                    return true;
                }
            });

            // language
            final ListPreference languagePreference = (ListPreference) getPreferenceScreen().getPreference(4);
            String defaultLanguage = languagePreference.getEntry().toString();
            languagePreference.setSummary(defaultLanguage);
            languagePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String newDefaultLanguage = (String) newValue;
                    int index = languagePreference.findIndexOfValue(newDefaultLanguage);
                    languagePreference.setSummary(languagePreference.getEntries()[index]);
                    if (!newValue.equals(Prefs.getLanguage(context))) {
                        Toast.makeText(context, getString(R.string.restart_hint), Toast.LENGTH_LONG).show();
                    }
                    return true;
                }
            });
        }
    }
}
