package com.krikunovapps.reminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.Locale;

public class ListActivity extends AppCompatActivity {

    public static AppCompatActivity instance;

    public static final String ACTION_UPDATE_LIST = "com.krikunovapps.reminder.list_activity.action_update_list";

    private Context mContext;
    private DatabaseHelper mDatabaseHelper;
    private BroadcastReceiver mReceiver;

    // ui
    private SectionedListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        instance = this;
        mContext = getApplicationContext();

        // Set locale
        String iso = Prefs.getLanguage(mContext);
        Locale locale = new Locale(iso, iso.toUpperCase());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getApplicationContext().getResources().getDisplayMetrics());

        // UI

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_top);
        toolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(toolbar);

        // Recycler view setup
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.alarms_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mDatabaseHelper = new DatabaseHelper(mContext);

        // List adapter
        mAdapter = new SectionedListAdapter(this,R.layout.list_section,R.id.section_text, mDatabaseHelper.getAll());

        // Delete on list item swipe
        ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(
                0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                if (viewHolder instanceof SectionedListAdapter.SectionViewHolder) return 0;
                return super.getSwipeDirs(recyclerView, viewHolder);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                  RecyclerView.ViewHolder target) {
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = mAdapter.sectionedPositionToPosition(
                        viewHolder.getAdapterPosition());
                final Alarm alarm = mAdapter.get(position);
                mAdapter.remove(alarm);
                mDatabaseHelper.delete(alarm);
                new AlarmManagerHelper(mContext).cancel(alarm);

                Intent i = new Intent(mContext, NotificationService.class);
                i.putExtra(Alarm.ALARM_EXTRA_NAME, alarm);
                i.setAction(NotificationService.ACTION_DISMISS);
                startService(i);

                // Confirmation bar
                final Snackbar snackbar = Snackbar
                        .make(findViewById(R.id.main_content), mContext.getString(R.string.snackbar_text), Snackbar.LENGTH_LONG)
                        .setAction(mContext.getString(R.string.snackbar_action), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Alarm newAlarm = mDatabaseHelper.create(alarm.getNote(), alarm.getDatetime());
                                new AlarmManagerHelper(mContext).set(newAlarm);
                                mAdapter.add(newAlarm);
                                mAdapter.notifyAlarmsDataSetChanged();
                            }
                        });
                snackbar.show();
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        recyclerView.setAdapter(mAdapter);

        // create alarm on button click
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, EditActivity.class);
                startActivity(i);
            }
        });

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mAdapter.refresh(mDatabaseHelper.getAll());
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_UPDATE_LIST);
        registerReceiver(mReceiver, filter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.alarm_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent i = new Intent(mContext, SettingsActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        instance = null;
        unregisterReceiver(mReceiver);
    }

}

