package com.krikunovapps.reminder;

import android.content.Context;

import org.joda.time.DateTime;

public class DateTimeFormatter {

    private final Context mContext;

    public DateTimeFormatter(Context context) {
        mContext = context;
    }

    public String formatTime(DateTime dt) {
        String minutes = dt.minuteOfHour().getAsText();
        if (minutes.length() == 1) {
            minutes = "0"+minutes;
        }
        return dt.getHourOfDay()+":"+minutes;
    }

    public String formatDate(DateTime dt) {
        if (isToday(dt)) {
            return mContext.getString(R.string.today);
        } else if (isTomorrow(dt)) {
            return mContext.getString(R.string.tomorrow);
        } else if (isWithinWeek(dt)) {
            String s = dt.dayOfWeek().getAsText();
            return s.substring(0,1).toUpperCase()+s.substring(1);
        } else {
            String s = dt.dayOfWeek().getAsShortText();
            s = s.substring(0,1).toUpperCase()+s.substring(1);
            return dt.getDayOfMonth() + " " + dt.monthOfYear().getAsShortText()+", "+s;
        }
    }

    private boolean isToday(DateTime dt) {
        return dt.toLocalDate().equals(new DateTime().toLocalDate());
    }

    private boolean isTomorrow(DateTime dt) {
        return dt.toLocalDate().minusDays(1).equals(new DateTime().toLocalDate());
    }

    private boolean isWithinWeek(DateTime dt) {
        return dt.toLocalDate().isBefore(new DateTime().plusDays(7).toLocalDate());
    }
}
