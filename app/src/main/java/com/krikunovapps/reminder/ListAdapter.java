package com.krikunovapps.reminder;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private SortedList<Alarm> mAlarms;
    private ViewHolder.AlarmClickListener mClickListener;

    Context mContext;
    DateTimeFormatter mDateTimeFormatter;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView mNote, mDate;
        private AlarmClickListener mClickListener;

        public ViewHolder(View v, AlarmClickListener listener) {
            super(v);
            mClickListener = listener;
            mNote = (TextView) v.findViewById(R.id.alarm_note);
            mDate = (TextView) v.findViewById(R.id.alarm_time);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mClickListener.onAlarmClick(v, getLayoutPosition());
        }

        public static interface AlarmClickListener {
            public void onAlarmClick(View caller, int pos);
        }
    }

    public ListAdapter(Context context, List<Alarm> alarms, ViewHolder.AlarmClickListener listener) {
        mClickListener = listener;
        mContext = context;
        mDateTimeFormatter = new DateTimeFormatter(mContext);
        mAlarms = new SortedList<>(Alarm.class, new SortedList.Callback<Alarm>() {
            @Override
            public int compare(Alarm o1, Alarm o2) {
                return (int) (o1.getDatetime() - o2.getDatetime());
            }

            @Override
            public void onInserted(int position, int count) {
                notifyItemRangeInserted(position, count);
            }

            @Override
            public void onRemoved(int position, int count) {
                notifyItemRangeRemoved(position, count);
            }

            @Override
            public void onMoved(int fromPosition, int toPosition) {
                notifyItemMoved(fromPosition, toPosition);
            }

            @Override
            public void onChanged(int position, int count) {
                notifyItemRangeChanged(position, count);
            }

            @Override
            public boolean areContentsTheSame(Alarm oldItem, Alarm newItem) {
                boolean idsAreSame = oldItem.getId() == newItem.getId();
                boolean notesAreSame = oldItem.getNote().equals(newItem.getNote());
                boolean datesAreSame = oldItem.getDatetime() == newItem.getDatetime();
                return idsAreSame && notesAreSame && datesAreSame;
                /*return areItemsTheSame(oldItem, newItem);*/
            }

            @Override
            public boolean areItemsTheSame(Alarm item1, Alarm item2) {
                return item1.getId() == item2.getId();
            }
        });
        mAlarms.addAll(alarms);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new ViewHolder(v, mClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Alarm alarm = mAlarms.get(position);
        holder.mNote.setText(alarm.getNote());
        holder.mDate.setText(mDateTimeFormatter.formatTime(new DateTime(alarm.getDatetime())));
        if (alarm.isNotified()) {
            holder.mNote.setTextColor(Color.RED);
        } else  {
            holder.mNote.setTextColor(Color.BLACK);
        }
    }

    @Override
    public int getItemCount() {
        return mAlarms.size();
    }

    public SortedList<Alarm> getAlarms() {
        return mAlarms;
    }

    public void clear() {
        mAlarms.clear();
    }
}
