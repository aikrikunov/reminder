package com.krikunovapps.reminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class AlarmManagerHelper {
    private Context mContext;
    private AlarmManager mAlarmManager;

    public AlarmManagerHelper(Context context) {
        this.mContext = context;
        this.mAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

    public void set(Alarm alarm) {
        mAlarmManager.set(AlarmManager.RTC_WAKEUP, alarm.getDatetime(), getAlarmPendingIntent(alarm));
    }

    public void update(Alarm alarm) {
        cancel(alarm);
        mAlarmManager.set(AlarmManager.RTC_WAKEUP, alarm.getDatetime(), getAlarmPendingIntent(alarm));
    }

    public void cancel(Alarm alarm) {
        mAlarmManager.cancel(getAlarmPendingIntent(alarm));
    }

    private PendingIntent getAlarmPendingIntent(Alarm alarm) {
        Intent i = new Intent(mContext, NotificationService.class);
        i.setAction(NotificationService.ACTION_START);
        i.putExtra(Alarm.ALARM_EXTRA_NAME, alarm);
        return PendingIntent.getService(mContext, alarm.getId(), i, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}
