package com.krikunovapps.reminder;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import org.joda.time.DateTime;

public class EditActivity extends AppCompatActivity implements
        DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static final String DATE_PICKER_TAG = "com.krikunovapps.reminder.edit_activity.date_picker";
    private static final String TIME_PICKER_TAG = "com.krikunovapps.reminder.edit_activity.time_picker";
    private static final String YEAR_TAG = "com.krikunovapps.reminder.edit_activity.year";
    private static final String MONTH_TAG = "com.krikunovapps.reminder.edit_activity.month";
    private static final String DAY_TAG = "com.krikunovapps.reminder.edit_activity.day";
    private static final String HOUR_TAG = "com.krikunovapps.reminder.edit_activity.hour";
    private static final String MINUTE_TAG = "com.krikunovapps.reminder.edit_activity.minute";

    private Context mContext;

    private EditText mNoteText;
    private TextView mDateText;
    private TextView mTimeText;
    private InputMethodManager imm;
    private DateTimeFormatter mDateTimeFormatter;

    private Alarm mAlarm;
    private String mNote;
    private DateTime mDatetime;

    private boolean mDialogShown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        mContext = getBaseContext();
        mDateTimeFormatter = new DateTimeFormatter(mContext);

        // data
        mAlarm = getIntent().getParcelableExtra(Alarm.ALARM_EXTRA_NAME);
        if (mAlarm != null) {
            mNote = mAlarm.getNote();
            mDatetime = new DateTime(mAlarm.getDatetime());
        } else {
            mNote = "";
            mDatetime = new DateTime()
                    .plusMinutes(Prefs.getDefaultTime(mContext))
                    .plusDays(Prefs.getDefaultDate(mContext));
        }

        // UI
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_top);
        setSupportActionBar(toolbar);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        mNoteText = (EditText) findViewById(R.id.note_text);
        mNoteText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }
            }
        });
        mNoteText.setText(mNote);
        mNoteText.requestFocus();

        mDateText = (TextView) findViewById(R.id.date_text);
        mDateText.setPaintFlags(mDateText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mDateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Prepare default data for picker
                Bundle bundle = new Bundle();
                bundle.putInt(YEAR_TAG, mDatetime.getYear());
                bundle.putInt(MONTH_TAG, mDatetime.getMonthOfYear() - 1);
                bundle.putInt(DAY_TAG, mDatetime.getDayOfMonth());

                // Picker
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.setArguments(bundle);
                newFragment.show(getSupportFragmentManager(), DATE_PICKER_TAG);
            }
        });
        mDateText.setText(mDateTimeFormatter.formatDate(mDatetime));

        mTimeText = (TextView) findViewById(R.id.time_text);
        mTimeText.setPaintFlags(mTimeText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mTimeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Prepare data for picker
                Bundle bundle = new Bundle();
                bundle.putInt(HOUR_TAG, mDatetime.getHourOfDay());
                bundle.putInt(MINUTE_TAG, mDatetime.getMinuteOfHour());

                DialogFragment newFragment = new TimePickerFragment();
                newFragment.setArguments(bundle);
                newFragment.show(getSupportFragmentManager(), TIME_PICKER_TAG);
            }
        });
        mTimeText.setText(mDateTimeFormatter.formatTime(mDatetime));

        Button readyButton = (Button) findViewById(R.id.ready_button);
        readyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseHelper db = new DatabaseHelper(mContext);
                AlarmManagerHelper am = new AlarmManagerHelper(mContext);
                mNote = mNoteText.getText().toString();
                if (mAlarm == null) {
                    mAlarm = db.create(mNote, mDatetime.getMillis());
                    am.set(mAlarm);
                } else {
                    mAlarm.setNote(mNote);
                    mAlarm.setDatetime(mDatetime.getMillis());
                    db.update(mAlarm);
                    am.update(mAlarm);
                }
                Intent i = new Intent(ListActivity.ACTION_UPDATE_LIST);
                sendBroadcast(i);
                hideKeyboard();
                finish();
            }
        });

        Button closeButton = (Button) findViewById(R.id.close_button);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                finish();
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        mDatetime = mDatetime
                .withYear(year)
                .withMonthOfYear(monthOfYear+1)
                .withDayOfMonth(dayOfMonth);
        mDateText.setText(mDateTimeFormatter.formatDate(mDatetime));
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        DateTime dateTime = mDatetime
                .withHourOfDay(hourOfDay)
                .withMinuteOfHour(minute)
                .withSecondOfMinute(0)
                .withMillisOfSecond(0);
        if (!dateTime.isBeforeNow()) {
            mDatetime = dateTime;
            mTimeText.setText(mDateTimeFormatter.formatTime(mDatetime));
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.wrong_time_message);
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();
                    mDialogShown = false;
                }
            });
            AlertDialog dialog = builder.create();
            if (!mDialogShown) {
                dialog.show();
                mDialogShown = true;
            }
        }
    }

    private void hideKeyboard() {
        imm.hideSoftInputFromWindow(mNoteText.getWindowToken(), 0);
        SystemClock.sleep(400);
    }

    public static class DatePickerFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Bundle bundle = this.getArguments();

            int year = bundle.getInt(YEAR_TAG);
            int month = bundle.getInt(MONTH_TAG);
            int day = bundle.getInt(DAY_TAG);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), R.style.PickerTheme, (EditActivity)getActivity(), year, month, day);
            dialog.getDatePicker().setMinDate(new DateTime().getMillis()-100);

            return dialog;
        }
    }

    public static class TimePickerFragment extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Bundle bundle = this.getArguments();

            int hour = bundle.getInt(HOUR_TAG);
            int minute = bundle.getInt(MINUTE_TAG);

            return new TimePickerDialog(getActivity(), R.style.PickerTheme, (EditActivity)getActivity(), hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }
    }

}
