package com.krikunovapps.reminder;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;

public class Prefs {

    private static final String PREF_NOTIFYING = "com.krikunovapps.reminder.preferences.preference_notifying";
    private static final String PREF_RINGTONE = "com.krikunovapps.reminder.preferences.preference_ringtone";
    private static final String PREF_SNOOZE_TIME = "com.krikunovapps.reminder.preferences.preference_snooze_time";
    private static final String PREF_DEFAULT_DATE = "com.krikunovapps.reminder.preferences.preference_default_date";
    private static final String PREF_DEFAULT_TIME = "com.krikunovapps.reminder.preferences.preference_default_time";
    private static final String PREF_LANGUAGE = "com.krikunovapps.reminder.preferences.preference_language";

    private static SharedPreferences getPrefs(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static boolean isNotifying(Context context) {
        SharedPreferences prefs = getPrefs(context);
        return prefs.getBoolean(PREF_NOTIFYING, false);
    }

    public static void setNotifying(Context context, boolean b) {
        SharedPreferences prefs = getPrefs(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREF_NOTIFYING, b);
        editor.commit();
    }

    public static Uri getRingtone(Context context) {
        SharedPreferences prefs = getPrefs(context);
        String strRingtonePreference = prefs.getString(PREF_RINGTONE, null);
        if (strRingtonePreference == null) {
            return RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        } else {
            return Uri.parse(strRingtonePreference);
        }
    }

    public static int getSnoozeTime(Context context) {
        SharedPreferences prefs = getPrefs(context);
        return Integer.parseInt(prefs.getString(PREF_SNOOZE_TIME, "5"));
    }

    public static int getDefaultDate(Context context) {
        SharedPreferences prefs = getPrefs(context);
        return Integer.parseInt(prefs.getString(PREF_DEFAULT_DATE, "0"));
    }

    public static int getDefaultTime(Context context) {
        SharedPreferences prefs = getPrefs(context);
        return Integer.parseInt(prefs.getString(PREF_DEFAULT_TIME, "60"));
    }

    public static String getLanguage(Context context) {
        SharedPreferences prefs = getPrefs(context);
        return prefs.getString(PREF_LANGUAGE, "en");
    }
}
