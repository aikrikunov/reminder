package com.krikunovapps.reminder;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;

import org.joda.time.DateTime;

public class NotificationService extends IntentService {

    public final static long TIMEOUT_TIME = 1*60*1000;

    public final static String ACTION_START = "com.krikunovapps.reminder.NotificationService.action_start";
    public final static String ACTION_DISMISS = "com.krikunovapps.reminder.NotificationService.action_dismiss";
    public final static String ACTION_SNOOZE = "com.krikunovapps.reminder.NotificationService.action_snooze";

    public final static boolean NOTIF_FLAG_DISMISS = true;
    public final static boolean NOTIF_FLAG_SNOOZE = false;

    private Context mContext;
    private Alarm mAlarm;
    private AlarmManager mAlarmManager;

    public NotificationService() {
        super("com.krikunovapps.reminder.notification_service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mContext = getApplicationContext();
        DatabaseHelper db = new DatabaseHelper(mContext);
        mAlarmManager = (AlarmManager) mContext.getSystemService(ALARM_SERVICE);

        // get alarm
        mAlarm = intent.getParcelableExtra(Alarm.ALARM_EXTRA_NAME);
        if (mAlarm == null) {
            mAlarm = db.getLatestNotified();
        }
        // check
        if (mAlarm == null) {
            return;
        }

        // act
        String action = intent.getAction();
        switch (action) {
            case ACTION_START:
                Intent i; PendingIntent p;
                // Cancel other scheduled events
                i = new Intent(mContext, NotificationService.class);
                i.setAction(ACTION_SNOOZE);
                p = PendingIntent.getService(mContext, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
                mAlarmManager.cancel(p);
                i = new Intent(mContext, NotificationService.class);
                i.setAction(ACTION_START);
                p = PendingIntent.getService(mContext, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
                mAlarmManager.cancel(p);

                if (!mAlarm.isNotified()) {
                    mAlarm.makeNotified();
                    mAlarm.setDatetime(new DateTime().getMillis());
                    db.update(mAlarm);
                    i = new Intent(ListActivity.ACTION_UPDATE_LIST);
                    sendBroadcast(i);
                }
                if (Prefs.isNotifying(mContext)) {
                    hideAlert();
                }
                issueNotification(NOTIF_FLAG_DISMISS);
                showAlert();
                Prefs.setNotifying(mContext, true);
                WakeLock.acquire(mContext);
                Klaxon.start(mContext);
                scheduleAutoSnooze();
                break;
            case ACTION_DISMISS:
                // Cancel auto snooze
                i = new Intent(mContext, NotificationService.class);
                i.setAction(ACTION_SNOOZE);
                p = PendingIntent.getService(mContext, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
                mAlarmManager.cancel(p);

                Klaxon.stop();
                WakeLock.release();
                cancelNotification();
                hideAlert();
                Prefs.setNotifying(mContext, false);
                db.delete(mAlarm);
                next();
                break;
            case ACTION_SNOOZE:
                snooze();
                Klaxon.stop();
                WakeLock.release();
                issueNotification(NOTIF_FLAG_SNOOZE);
                hideAlert();
                Prefs.setNotifying(mContext, false);
                break;
        }
        Intent i = new Intent(ListActivity.ACTION_UPDATE_LIST);
        sendBroadcast(i);
    }

    private void next() {
        Intent i = new Intent(mContext, NotificationService.class);
        i.setAction(ACTION_START);
        SystemClock.sleep(100);
        startService(i);
    }

    private void scheduleAutoSnooze() {
        Intent i = new Intent(mContext, NotificationService.class);
        i.setAction(ACTION_SNOOZE);
        PendingIntent p = PendingIntent.getService(mContext, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        mAlarmManager.set(AlarmManager.RTC_WAKEUP, new DateTime().getMillis() + TIMEOUT_TIME, p);
    }

    private void snooze() {
        Intent i = new Intent(mContext, NotificationService.class);
        i.setAction(ACTION_START);
        PendingIntent p = PendingIntent.getService(mContext, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        mAlarmManager.set(AlarmManager.RTC_WAKEUP, new DateTime().getMillis() + Prefs.getSnoozeTime(mContext)*60*1000, p);
    }

    private void issueNotification(boolean flags) {
        NotificationManager nm = (NotificationManager)
                mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent contentIntent = new Intent(mContext, AlertActivity.class);
        contentIntent.putExtra(Alarm.ALARM_EXTRA_NAME, mAlarm);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(mContext, 0, contentIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent doneIntent = new Intent(mContext, NotificationService.class);
        doneIntent.setAction(ACTION_DISMISS);
        PendingIntent donePendingIntent = PendingIntent.getService(mContext, 0, doneIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent snoozeIntent = new Intent(mContext, NotificationService.class);
        snoozeIntent.setAction(ACTION_SNOOZE);
        PendingIntent snoozePendingIntent = PendingIntent.getService(mContext, 0, snoozeIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder nb = new NotificationCompat.Builder(mContext);
        nb.setSmallIcon(R.drawable.ic_notif)
                .setContentTitle(mAlarm.getNote())
                .setOngoing(true)
                .setContentIntent(contentPendingIntent)
                .setVibrate(new long[0]);

        if (flags) {
            nb.setPriority(NotificationCompat.PRIORITY_MAX)
                    .addAction(R.drawable.ic_snooze_action, getString(R.string.snooze), snoozePendingIntent);
        }
        nb.addAction(R.drawable.ic_done_action, getString(R.string.dismiss), donePendingIntent);

        // issue
        nm.notify(1, nb.build());
    }

    private void cancelNotification() {
        NotificationManager nm = (NotificationManager)
                mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(1);
    }

    private void showAlert() {
        KeyguardManager km = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        if (km.inKeyguardRestrictedInputMode()) {
            if (AlertActivity.instance == null) {
                Intent i = new Intent(mContext, AlertActivity.class);
                i.putExtra(Alarm.ALARM_EXTRA_NAME, mAlarm);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        }
    }

    private void hideAlert() {
        if (AlertActivity.instance != null) {
            AlertActivity.instance.finish();
        }
    }
}
