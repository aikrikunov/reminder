package com.krikunovapps.reminder;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import org.joda.time.DateTime;

import java.util.ArrayList;

public class BootService extends IntentService {
    public BootService() {
        super("com.krikunovapps.reminder.boot_service");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final Context mContext = getApplicationContext();
        DatabaseHelper db = new DatabaseHelper(mContext);
        AlarmManagerHelper am = new AlarmManagerHelper(mContext);

        ArrayList<Alarm> alarms = (ArrayList<Alarm>) db.getAll();
        for (Alarm alarm : alarms) {
            DateTime dt = new DateTime(alarm.getDatetime());
            if (dt.isBeforeNow()) {
                SystemClock.sleep(500);
                Intent i = new Intent(mContext, NotificationService.class);
                i.setAction(NotificationService.ACTION_START);
                i.putExtra(Alarm.ALARM_EXTRA_NAME, alarm);
                startService(i);
            } else {
                am.set(alarm);
            }
        }
    }
}
