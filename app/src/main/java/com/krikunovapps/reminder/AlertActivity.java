package com.krikunovapps.reminder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

public class AlertActivity extends AppCompatActivity {

        public static AppCompatActivity instance;
        private Context mContext;

        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        instance = this;
        mContext = getApplicationContext();

        getWindow().addFlags(
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

        setContentView(R.layout.activity_alert);

        TextView note = (TextView) findViewById(R.id.alert_note);
        Alarm alarm = getIntent().getParcelableExtra(Alarm.ALARM_EXTRA_NAME);
        note.setText(alarm.getNote());
    }

    public void dismiss(View v) {
        Intent i = new Intent(mContext, NotificationService.class);
        i.setAction(NotificationService.ACTION_DISMISS);
        startService(i);
        finish();
    }

    public void snooze(View v) {
        Intent i = new Intent(mContext, NotificationService.class);
        i.setAction(NotificationService.ACTION_SNOOZE);
        startService(i);
        finish();
    }

    @Override
    public void onBackPressed() {}

    @Override
    protected void onDestroy() {
        super.onDestroy();
        instance = null;
        WakeLock.release();
    }
}
